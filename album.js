/* 
 * Album - an album in the collection, with an album image
 */

function CopyCoverAlbum(albumId) {
    this.id = albumId;
    this.name = Amarok.Collection.query("SELECT name FROM albums WHERE id="+this.id);
    this.image = this.getImage();
    this.path = this.getPath();
    this.artist = this.getArtist();
    this.filename = this.getFileName();
}
CopyCoverAlbum.prototype.getImage = function() { 
    var sql = "SELECT path FROM images " 
             +"LEFT JOIN albums ON images.id = albums.image " 
             +"WHERE albums.id = " + this.id;
    return Amarok.Collection.query(sql).toString();
}
CopyCoverAlbum.prototype.getPath = function() { 
    var sql = "SELECT DISTINCT D.dir, D.deviceid FROM directories AS D "
             +"LEFT JOIN urls ON D.id = urls.directory "
             +"LEFT JOIN tracks ON tracks.url = urls.id "
             +"WHERE tracks.album = " + this.id;
    var result = Amarok.Collection.query(sql);
    var devicePath;
    if (result[1] == -1) {
        devicePath = "";
    } else {
        devicePath = Amarok.Collection.query(
                        "SELECT lastmountpoint FROM devices WHERE id="+result[1]);
    }
    return devicePath + "/" + result[0];
}
CopyCoverAlbum.prototype.getArtist = function() { 
    var sql = "SELECT artists.name FROM artists " 
             +"LEFT JOIN albums ON artists.id = albums.artist " 
             +"WHERE albums.id = " + this.id;
    return Amarok.Collection.query(sql).toString();
}
CopyCoverAlbum.prototype.getFileName = function() { 
    var dir = new QDir(this.path);
    var filename = "cover.jpg";
    var usealbumname = Amarok.Script.readConfig("usealbumname", "false");
    if (usealbumname == "true") {
        // Keep filenames simple
        filename = this.name.toString().replace(/[^a-zA-Z0-9_-]/g,'') + ".jpg";
    } else {
        filename = Amarok.Script.readConfig("filename", "cover.jpg");
        if (!filename.match(/.*\.jpe?g$/)) {
            // We only handle JPEG files
            filename = filename + ".jpg";
        }
    }
    return dir.filePath(filename);
}
CopyCoverAlbum.prototype.needsCopy = function() { 
    var albumDir = new QDir(this.path);
    albumDir.setNameFilters(["*.png", "*.jpg", "*.gif", "*.jpeg"]);
    var albumImages = albumDir.entryList();
    if (albumImages.length > 0) {
        return false;
    } else {
        return true;
    }
}
CopyCoverAlbum.prototype.needsDesktopFile = function() { 
    var albumDir = new QDir(this.path);
    var df = new QFile(this.path + "/.directory");
    return !df.exists();
}
CopyCoverAlbum.prototype.copyImage = function() { 
    var imageFile = new QFile(this.image);
    //Amarok.debug("Copying cover for album \""+this.artist+"\" - \""+this.name+"\" (id "+this.id+")"); return;
    if (imageFile.copy(this.filename)) {
        Amarok.debug("Image for album '"+this.name+"' (id "
                    +this.id+") was successfully copied !");
        Amarok.Window.Statusbar.shortMessage("Copied cover of \""+this.name
                                            +"\" by \""+this.artist+"\".");
        if (Amarok.Script.readConfig("desktopfile", "false") == "true") {
            this.addCoverToDesktopFile();
        }
        return true;
    } else {
        Amarok.debug("Image for album '"+this.name+"' (id "
                    +this.id+") could not be copied !");
        Amarok.Window.Statusbar.shortMessage("Could not copy cover of \""+this.name
                                            +"\" by \""+this.artist+"\" !");
        return false;
    }
}
CopyCoverAlbum.prototype.addCoverToDesktopFile = function() {
    var filename = new QFileInfo(this.filename);
    filename = filename.fileName();
    return this.createDesktopFile(filename);
}
CopyCoverAlbum.prototype.addExistingImageToDesktopFile = function() {
    var albumDir = new QDir(this.path);
    albumDir.setNameFilters(["*.png", "*.jpg", "*.gif", "*.jpeg"]);
    var albumImages = albumDir.entryList();
    if (albumImages.length == 1) {
        return this.createDesktopFile(albumImages[0]);
    }
    if (albumImages.length > 1) {
        // Try to be smart in choosing the cover
        preferred = ["cover", "front"];
        for (var p=0; p < preferred.length; p++) {
            preferredRe = new RegExp(".*"+preferred[p]+".*", "i");
            for (var i=0; i < albumImages.length; i++) {
                if (albumImages[i].match(preferredRe)) {
                    return this.createDesktopFile(albumImages[i]);
                }
            }
        }
        // I'd love to be able to match the filenames to which cover we have in
        // database for this album, but I can't find a way.
        // Oh well, use the first one.
        return this.createDesktopFile(albumImages[0]);
    }
    return false;
}
CopyCoverAlbum.prototype.createDesktopFile = function(filename) {
    var dir = new QDir(this.path);
    /* Using QSettings does not work because it escapes the group names
       and therefore cannot create a "Desktop Entry" group. It is created
       as Desktop%20Entry. There are others aspects of the spec which are
       not honored in the IniFormat format. A solution would be to have
       access to the DesktopFormat defined by KDE.
    */
    /*
    var de = new QSettings(dir.filePath(".directory"), QSettings.IniFormat);
    de.beginGroup("Desktop Entry");
    if (de.contains("Icon")) {
        return false;
    }
    de.setValue("Icon", "./"+filename);
    de.endGroup();
    */
    var df = new QFile(dir.filePath(".directory"));
    if (df.exists()) {
        return false;
    }
    Amarok.debug("Creating desktop file for album \""+this.name+"\" by \""
                +this.artist+"\" with the cover \""+filename+"\".");
    df.open(new QIODevice.OpenMode(QIODevice.WriteOnly));
    var ts = new QTextStream(df);
    ts.writeString("[Desktop Entry]\nIcon=./" + filename + "\n");
    df.close();
}
CopyCoverAlbum.prototype.process = function() {
    /* results:
     * 0: nothing to do
     * 1: failed
     * 2: copied
     * 3: created desktop file
     */
    var result = 1;
    if (this.needsCopy()) {
        if (this.copyImage()) {
            result = 2;
        }
    } else { // image already in directory
        if (Amarok.Script.readConfig("desktopfile", "false") == "true"
                && this.needsDesktopFile()) {
            if (this.addExistingImageToDesktopFile()) {
                result = 3;
            }
        } else {
            result = 0;
        }
    }
    return result;
}


/*
 * The configuration dialog
 */

function CopyCoverConfigure() {
    QMainWindow.call(this, null);
    var mainWidget = new QWidget(this);
    this.windowTitle = "Copy cover script";
    this.windowIcon = "media-album-cover";
    this.setWindowFlags(Qt.WA_DeleteOnClose);
    this.coverNameLabel = new QLabel("What filename should I use for album covers ?", mainWidget);
    //this.coverNameLabel.size = new QSize(600, 50);
    //this.coverNameLabel.sizeHint = new QSize(600, 50);
    this.albumName = new QCheckBox("Use album &name", mainWidget);
    this.fileNameLabel = new QLabel("Always use this &filename: ", mainWidget);
    this.fileNameEdit = new QLineEdit(mainWidget);
    this.fileNameLabel.setBuddy(this.fileNameEdit);
    this.albumName.toggled.connect(this, "toggleAlbumName");
    this.desktopFile = new QCheckBox("Use the cover image as folder icon", mainWidget);
    this.whilePlaying = new QCheckBox("Copy covers &while playing", mainWidget);
    this.okButton = new QPushButton("OK", mainWidget);
    this.okButton["default"] = true;
    this.okButton.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed);
    this.okButton.clicked.connect(this, "saveConf");
    var layout = new QGridLayout(mainWidget);
    layout.setContentsMargins(5, 20, 5, 5);
    layout.setSpacing(10);
    layout.addWidget(this.coverNameLabel, 0, 0, 1, 3);
    layout.addWidget(this.albumName, 1, 0, 1, 3);
    layout.addWidget(this.fileNameLabel, 2, 0);
    layout.addWidget(this.fileNameEdit, 2, 1, 1, 2);
    layout.addWidget(this.desktopFile, 3, 0, 1, 3);
    layout.addWidget(this.whilePlaying, 4, 0, 1, 3);
    layout.addWidget(this.okButton, 6, 2, 1, 1, Qt.AlignRight);
    mainWidget.setLayout(layout);
    this.setCentralWidget(mainWidget);
    //this.resize(500, 200);
    this.loadConf();
    this.show();
}
CopyCoverConfigure.prototype = new QMainWindow();
CopyCoverConfigure.prototype.toggleAlbumName = function() {
    this.fileNameEdit.enabled = (this.albumName.checkState() == Qt.Unchecked)
}
CopyCoverConfigure.prototype.loadConf = function() {
    var useAlbum = Amarok.Script.readConfig("usealbumname", "false");
    var fileName = Amarok.Script.readConfig("filename", "cover.jpg");
    if (useAlbum == "true") {
        this.albumName.setCheckState(Qt.Checked);
        this.fileNameEdit.enabled = false;
    } else {
        this.albumName.setCheckState(Qt.Unchecked);
        this.fileNameEdit.enabled = true;
    }
    this.fileNameEdit.text = fileName;
    if (Amarok.Script.readConfig("desktopfile", "false") == "true") {
        this.desktopFile.setCheckState(Qt.Checked);
    }
    if (Amarok.Script.readConfig("whileplaying", "false") == "true") {
        this.whilePlaying.setCheckState(Qt.Checked);
    }
}
CopyCoverConfigure.prototype.saveConf = function() {
    var useAlbum = (this.albumName.checkState() == Qt.Checked);
    if (!useAlbum && !this.fileNameEdit.text.match(/.*\.jpe?g$/)) {
        Amarok.alert("The filename must end in \".jpg\" !", "sorry");
        return;
    }
    Amarok.Script.writeConfig("usealbumname", useAlbum.toString());
    Amarok.Script.writeConfig("filename", this.fileNameEdit.text);
    var desktopFile = (this.desktopFile.checkState() == Qt.Checked);
    Amarok.Script.writeConfig("desktopfile", desktopFile.toString());
    var whilePlaying = (this.whilePlaying.checkState() == Qt.Checked);
    Amarok.Script.writeConfig("whileplaying", whilePlaying.toString());
    this.close();
}


//Amarok.Window.Statusbar.setMainText( Amarok.Playlist.totalTrackCount() + " tracks in the playlist");
//Amarok.Window.OSD.setText("TestOSD");
//Amarok.Window.OSD.show();

Importer.loadQtBinding("qt.core");
Importer.loadQtBinding("qt.gui");

Importer.include("album.js");
Importer.include("config.js");
Importer.include("processing.js");

function copycover_onchange() {
    try {
        // Always connect the signal and check here to be active right after the
        // user set the preference in the config dialog.
        if (Amarok.Script.readConfig("whileplaying", "false") != "true") {
            return;
        }
        copycover_current_track();
    } catch (e) {
        Amarok.alert("CopyCover crashed with message: \""+e+"\".\n"
                    +"This should not happen, please report the bug.", "error");
    }
}

function copycover_button() {
    try {
        var answer = Amarok.alert("This will run CopyCover on your whole music collection.\n"
                                 +"Depending on the size of your music collection, it may take some time,\n"
                                 +"slow down your computer, and use up more power than usual. Continue ?\n",
                                 "questionYesNo");
        if (answer == 3) {
            copycover_collection();
        }
    } catch (e) {
        Amarok.alert("CopyCover crashed with message: \""+e+"\".\n"
                    +"This should not happen, please report the bug.", "error");
    }
}

function copycover_configure() {
    try {
        var conf_window = new CopyCoverConfigure();
    } catch (e) {
        Amarok.alert("CopyCover crashed with message: \""+e+"\".\n"
                    +"This should not happen, please report the bug.", "error");
    }
}

try {
    Amarok.Window.addToolsSeparator();
    Amarok.Window.addToolsMenu("script_copycover", "Copy Cover", "media-album-cover");
    Amarok.Window.ToolsMenu.script_copycover["triggered()"].connect(copycover_button);

    Amarok.Window.addSettingsSeparator();
    Amarok.Window.addSettingsMenu("script_copycover_conf", "Configure Copy Cover...", "media-album-cover");
    Amarok.Window.SettingsMenu.script_copycover_conf["triggered()"].connect(copycover_configure);

    Amarok.Engine.trackChanged.connect(copycover_onchange);
    //Amarok.Playlist.activeRowChanged.connect(copycover_onchange);
} catch (e) {
    Amarok.alert("CopyCover crashed with message: \""+e+"\".\n"
                +"This should not happen, please report the bug.", "error");
}


Importer.include("album.js");

/*
 * Copy the covers for the whole collection
 */

function copycover_collection() {
    //var limit = 50;
    //var numAlbums = limit;
    var numAlbums = parseInt(Amarok.Collection.query(
                        "SELECT COUNT(*) FROM albums WHERE image IS NOT NULL"));
    Amarok.debug("There are "+numAlbums+" albums to analyse.");
    var progress = new QProgressDialog("Copying covers...", "Cancel", 0, numAlbums);
    progress.windowTitle = "Copy cover script";
    progress.autoClose = true;
    var label = "Copying covers...";
    if (Amarok.Script.readConfig("desktopfile", "false") == "true") {
        label = "Copying covers and updating icons...";
    }
    progress.setLabel(new QLabel(label));
    //var sql = "SELECT id FROM albums WHERE image IS NOT NULL ORDER BY id DESC LIMIT "+limit;
    var sql = "SELECT id FROM albums WHERE image IS NOT NULL ORDER BY id DESC";
    var albums = Amarok.Collection.query(sql);
    var copyIsOK = 0;
    var copyIsKO = 0;
    for (var i=0 ; i < albums.length ; i++) {
        var albumId = albums[i];
        progress.setValue(i);
        if (progress.wasCanceled) {
            break;
        }
        var album = new CopyCoverAlbum(albumId);
        //Amarok.debug("Analizing album \"" + album.name + "\" by \"" + album.artist + "\" (id " + albumId + ")");
        var result = album.process();
        if (result == 2) {
            copyIsOK += 1;
        } else if (result == 1) {
            copyIsKO += 1;
        }
    }
    progress.setValue(numAlbums);
    var message = "Copied "+ (copyIsOK+copyIsKO) +" covers. \n"
                 +copyIsOK+" were successful and "
                 +copyIsKO+" failed."
    Amarok.Window.Statusbar.longMessage(message);
    //Amarok.alert(message);
}

function copycover_current_track() {
    if (Amarok.Engine.engineState() != 0) {
        // "Playing" is 0, see:
        // http://kollide.net:8060/browse/Amarok/src/scriptengine/AmarokEngineScript.h?r=HEAD
        return;
    }
    var track = Amarok.Engine.currentTrack();
    if (!track.inCollection) {
        return;
    }
    var sql = "SELECT albums.id FROM albums "
             +"LEFT JOIN artists ON albums.artist = artists.id "
             +"WHERE albums.name = '"
             +Amarok.Collection.escape(track.album)
             +"' AND artists.name = '"
             +Amarok.Collection.escape(track.artist)+"'";
    try {
        var albumId = parseInt(Amarok.Collection.query(sql));
    } catch (e) {
        Amarok.debug("Can't find the album \""+track.album+"\" by \""+track.artist+"\". Maybe multiple albums with the same name ?");
        return;
    }
    var album = new CopyCoverAlbum(albumId);
    return album.process();
}

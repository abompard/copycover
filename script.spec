[Desktop Entry]
Icon=media-album-cover
Type=script
ServiceTypes=KPluginInfo

Name=Copy Cover
Comment=A script that backs up the album images in the album's directory

X-KDE-PluginInfo-Name=Copy Cover
X-KDE-PluginInfo-Version=1.0
X-KDE-PluginInfo-Category=Generic
X-KDE-PluginInfo-Author=Aurélien Bompard
X-KDE-PluginInfo-Email=aurelien@bompard.org
X-KDE-PluginInfo-Depends=Amarok2.0
X-KDE-PluginInfo-License=GPL
X-KDE-PluginInfo-EnabledByDefault=false

